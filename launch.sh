#!/bin/bash

# Terminate already running bar instances
pgrep -x polybar > /dev/null && killall -q polybar > /dev/null 2>&1

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null 2>&1
do
    sleep 1
done

# Launch Polybar, using default config location ~/.config/polybar/config
#polybar mybar &
# polybar bar-for-bspwm &  > /dev/null 2>&1
polybar bar-for-bspwm --log=error &
pgrep -x dunst > /dev/null && notify-send 'POLYBAR:' 'restarted...'

